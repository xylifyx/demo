package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping
    public String helloWorld() {
        return "<html>" +
                "<body>" +
                "<h1>Hello</h1>" +
                "<p>World</p>" +
                "</body>" +
                "</html>";
    }
}
